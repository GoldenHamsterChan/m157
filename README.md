﻿# Arbeitsauftrag Webcam für die Schüler

######  Autoren Sebastian Leibundgut & Sandro Filisetti
###### Version 1.0
---
## Einführung
Bei diesem Werkstattauftrag werdet Ihr lernen wie man eine USB-Webcam an den Raspi anschliesst und den Raspi so konfiguriert sodass die Kamera jede Minute automatisch ein Bild macht. Ihr solltet zuerst mal alles vorbereiten das ihr braucht, das benötigte Material findet ihr weiter unten in der Materialliste. Dieser Arbeitsauftrag sollte eigentlich in ca. 2 Lektionen erledigt sein ausser man würde auf sehr unerwartete Stolpersteine trifft z.B. Defekte Kamera oder Defekter USB-Port an Raspi. 


## Materialliste
### Hardware 
Kamera mit einem USB- Anschluss welcher kompatibel mit dem Raspi ist.

### Software
VNC Viewer -> Auf Raspi und auf dem eigenen Gerät installieren.
VNC Server Server wird nur auf dem eigenen Gerät benötigt.
AUf dem Raspi  wird das Paket fswebcam benötigt.  (Sudo apt-get fswebcam)
Aktuellstes OS und Firmware für den Raspi, hier gibt es keine Limite.

## Installationsanleitung
Die Schüler müssen folgende Schritte bewältigen damit eine benutzung der Webcam gewährleistet wird.

Das Paket namens fswebcam installieren

Der Benutzer muss einer speziellen Gruppe hinzugefügt werden, hier solltet Ihr beachten das ihr den Gruppenname richtig schreibt und das ihr den Benutzername ebenfalls richtig schreibt (Da es ein Linux-Basiertes Betriebssystem ist achtet euch auf die Gross-Kleinschreibung)

Testen ob es ein Bild aufnehmen kann, vergesst hier nicht den Namen des Bildes ebenfalls hinzuzufügen dies wird so gemacht:

		"fswebcam image.jpg"
	
(Dadurch wird ein jpg aufgenommen und mit dem Namen image.jpg gespeichert)

Bei korrekter ausführung sollte folgender Text abgebildet werden:

![](images/Bild4.png)

Wollen wir nun einschalten, dass jede Minute automatisch ein Bild aufgenommen wird. Kann dies mit einem Bash Skript ausgeführt werden, dazu wird aber noch ein Verzeichnis benötigt. Vorzugsweise wird es mit "Bilder" oder "Webcam" bennant.

Cmds= mkdir webcam ( für den Ordner)
Cmds= sudo nano (zum erstellen des Skriptes)

Die erste Linie im Skript sollte deklarieren das es ein Bash Skript ist: 
		
		#!/bin/bash
Danach solltet ihr es so machen das dass Bild mit einer Auflösung (engl. Resolution) von 1280x720 und dann noch das es ohne ein Banner photographiert wird. Das Bild sollte in eurem neu erstelltem Ordner gespeichert werden und so das keine Namenkonflikte gibt sollte es mit dem momentanem Datum und der momentaner Zeit (am besten eine variable erstellen und diese mit Datum + Zeit konfigurieren) gespeichert werden.

Die Datei sollten mit Namen wie z.B. webcam.sh gespeichert werden.

Die Fotos werden automatisch mit dem Namen des jeweiligen Datums gespeichert, 

Zum ausführen des Skriptes muss nur noch ./webcam.sh 
in das Terminal eingegeben werden.

Jetzt wollen wir das ganze noch so machen das es jede Minute ein Bild macht, hierzu könnt ihr auf dieser Seite: https://crontab.guru/#0_22_*_*_1-5 versuchen es so einzustellen das es "jede Minute" anzeigt. 

Wenn Ihr das dann so habt könnt Ihr über crontab die Benutzer einstellungen anpassen.

Wenn Ihr in crontab drin seit müsst ihr es so einstellen das jede Minute euer Bash Skript ausgeführt wird.

Wir verwenden Nano, da dies unser Präferierter Editor ist.

Wir müssen dem File sagen, was es ausführen soll.
Das wird mit der Folgenden Eingabe getätig.

    ["Code" für jede Minute von der Website] /home/pi/webcam.sh

## Qualitätskontrolle
Die Qualitätskontrolle ist eigentlich ganz Simpel:
Achtet euch darauf das es auch wirklich ein Bild macht
Ausserdem solltet Ihr schauen das neue Bilder die alten Bilder nicht überschreiben.
## Error Handling
Error Nachrichten sind unser Freund und Helfer, falls Ihr eine Fehlermeldung bekommt versucht euch wichtige Stichworte herauszufischen und dann falls Ihr nichts daraus erkennen könnt versucht zu Googlen (Google ist der beste Freund eines Informatikers).

